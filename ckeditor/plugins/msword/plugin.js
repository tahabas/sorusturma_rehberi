﻿/*
 Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
*/
//CKEDITOR.plugins.add('msword',
//{
//    init: function (editor) {
//        var pluginName = 'msword';
//        editor.ui.addButton('Msword',
//            {
//                label: 'Word Olarak Kaydet',
//                command: 'ExportWord',
//                icon: CKEDITOR.plugins.getPath('msword') + 'docx.png'
//            });
//        var cmd = editor.addCommand('ExportWord', { exec: printDiv });
//    }
//});


CKEDITOR.plugins.add( 'msword', {
    icons: 'msword',
    init: function( editor ) {
        editor.addCommand( 'ExportWord', {
            exec: function( editor ) {
                var data = CKEDITOR.instances.editor.getData();
            $div = j3('<div>', {class: 'spinner'});
            $div.html(data);
            $div.wordExport(filename);
            }
        });
        editor.ui.addButton( 'Msword', {
            label: 'Word Olarak Kaydet',
            command: 'ExportWord',
            toolbar: 'document'
        });
    }
});


